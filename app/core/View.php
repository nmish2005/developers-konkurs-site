<?php

namespace app\core;


class View
{

    function render($fileName, $dataProvider = [], $tempate = 'layout'){
        include_once $_SERVER['DOCUMENT_ROOT'] . '/developers-konkurs-site/app/views/' . $tempate . '.php';
    }

}