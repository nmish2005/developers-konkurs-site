<?php

namespace app\core;


class App
{

    static function addCss($arr)
    {
        Settings::getInstance()->param(
            'css',
            array_merge(
                Settings::getInstance()->param('css'),
                (array)$arr
            )
        );

        return true;
    }

    static function addLess($arr)
    {
        Settings::getInstance()->param(
            'less',
            array_merge(
                Settings::getInstance()->param('less'),
                (array)$arr
            )
        );

        return true;
    }

    static function addJs($arr)
    {
        Settings::getInstance()->param(
            'js',
            array_merge(
                Settings::getInstance()->param('js'),
                (array)$arr
            )
        );

        return true;
    }

    static function genCss()
    {
        $output = '';

        foreach (Settings::getInstance()->param('css') as $item) {
            $output .= '<link rel="stylesheet" href="/developers-konkurs-site/assets/css/' . $item . '.css">';
        }

        return $output;
    }

    static function genJs()
    {
        $output = '';

        foreach (Settings::getInstance()->param('js') as $item) {
            $output .= '<script src="/developers-konkurs-site/assets/js/' . $item . '.js"></script>';
        }

        return $output;
    }

    static function getUriComponents(){
        $uriComponents = array_filter(explode('/', $_SERVER['REQUEST_URI']));

        unset($uriComponents[1]);

        return array_values($uriComponents);

    }

    static function genLess()
    {
        $output = '';

        foreach (Settings::getInstance()->param('less') as $item) {
            $output .= '<link rel="stylesheet/less" href="/developers-konkurs-site/assets/less/' . $item . '.less">';
        }

        return $output;
    }

}