<?php

namespace app\core;


class Settings
{

    private static $_instance = null;
    private $settings = [
        'projectName' => 'Вибір професії',
        'projectUrl' => 'http://mike.sudoku.in.ua/developers-concurs-site/',
        'less' => [
            'main'
        ],
        'css' => [
            '../libs/bs/dist/css/bootstrap.min',
            '../libs/animate',
            '../libs/fa/css/font-awesome.min',
            '../libs/swal2/sweetalert2.min'
        ],
        'js' => [
            '../libs/jq',
            '../libs/popper',
            '../libs/bs/dist/js/bootstrap.min',
            '../libs/swal2/sweetalert2.min',
            '../libs/less.min',
            'main'
        ]
    ];

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self;

        return self::$_instance;
    }

    function getParamFromSettings($paramName)
    {
        return isset($this->settings[$paramName]) ? $this->settings[$paramName] : null;
    }

    function param($paramName, $value = null)
    {
        if (is_null($value)) {
            return isset($this->settings[$paramName]) ? $this->settings[$paramName] : null;
        }

        return $this->settings[$paramName] = $value;
    }
}