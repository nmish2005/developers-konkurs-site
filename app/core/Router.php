<?php

namespace app\core;


class Router
{

    static function Launch()
    {

        $uriComponents = App::getUriComponents();

        $controllerName = isset($uriComponents[0]) ? $uriComponents[0] : 'main';
        $actionName = isset($uriComponents[1]) ? $uriComponents[1] : 'index';

        $controllerName = ucfirst($controllerName) . 'Controller';
        $actionName = 'action_' . strtolower($actionName);

        $controllerPath = 'app\\controllers\\' . $controllerName;

        if (class_exists($controllerPath))
            $controller = new $controllerPath;
        else
            return Router::ErrorPage404();

        if (method_exists($controller, $actionName))
            $controller->$actionName();
        else
            return Router::ErrorPage404();

        return true;
    }

    static function ErrorPage404()
    {

        http_response_code(404);

        $mainController = new \app\controllers\MainController;

        $mainController->action_404();

        return 404;
    }

}