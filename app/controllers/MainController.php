<?php

namespace app\controllers;


use app\core\App;
use app\core\Controller;

class MainController extends Controller
{

    function action_index()
    {
        $this->View->render('main');
    }

    function action_quiz()
    {
        App::addJs('quiz');

        $this->View->render('quiz');
    }

    function action_404()
    {
        $this->View->render('errors/404');
    }

    function action_info()
    {
        phpinfo();
    }

    function action_test()
    {
        echo 'test page';
    }

    function action_pull(){
        $this->View->render('pull');
    }

    function action_quizResults(){
        $this->View->render('quizResults');
    }
}