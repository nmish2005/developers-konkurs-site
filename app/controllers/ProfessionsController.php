<?php

namespace app\controllers;


use app\core\Controller;

class ProfessionsController extends Controller
{

    function action_tester()
    {
        $this->View->render('professions/tester');
    }

    function action_hrSpec()
    {
        $this->View->render('professions/hrSpec');
    }

    function action_sysAdmin(){
        $this->View->render('professions/sysAdmin');
    }

    function action_itSupport(){
        $this->View->render('professions/itSupport');
    }

    function action_programmer()
    {
        $this->View->render('professions/programmer');
    }

}