<?php

namespace app\controllers;


use app\core\App;
use app\core\Controller;

class TechController extends Controller
{

    function action_web(){
        App::addJs('disqus');
        App::addLess('tree');

        $this->View->render('tech/web');
    }

    function action_mobile(){
        App::addJs('disqus');
        App::addLess('tree');

        $this->View->render('tech/mobile');
    }

    function action_system(){
        App::addJs('disqus');
        App::addLess('tree');

        $this->View->render('tech/system');
    }

}