<?php

namespace app\views\professions;

?>

<h1>Хто такий програміст?</h1>

<hr>

<div class="clearfix"></div>

<img src="/developers-konkurs-site/assets/img/programmer.png" class="rounded img-fluid profession"
     alt="Programer photo">

<div class="clearfix"></div>

<p>
    Програмісти - це люди, які пишуть програми для комп'ютерів. Як всім відомо, чи майже всім, то без програми комп'ютер працювати не може.
    Без програми комп'ютер не зможе помножити два на два, навіть якщо у нього, два, чотири або більше ядер.
</p>

<p>
    У програмуванні на перше місце ставляться не тільки практичні навички, а й ідеї фахівця.
    Програмістів можна умовно розділити на три категорії в залежності від спеціалізації:
</p>

<section class="wikiBlock">
    <div class="container py-3">
        <div class="row">
            <div class="wikiLogo col-lg-1 col-md-1 col-sm-12">
                <img src="/developers-konkurs-site/assets/img/ukwiki.png" class="w-100">
            </div>
            <div class="col-lg-11 col-md-11 col-sm-12">
                <div>
                    <h5>Програміст</h5>
                    <p>фахівець, що займається програмуванням, виконує розробку програмного забезпечення (в простіших
                        випадках — окремих програм) для програмованих пристроїв, які, як правило містять один чи більше
                        CPU. Прикладами таких пристроїв є: настільні персональні комп'ютери, мобільні телефони,
                        смартфони, комунікатори, ігрові приставки, сервери, суперкомп'ютери, мікроконтролери та
                        промислові комп'ютери.</p>
                    <div class="rightAligned">
                        <a href="https://uk.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D1%96%D1%81%D1%82"
                           target="_blank">Читати далі &raquo;</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<ul class="nums">
    <li><strong>Прикладні програмісти</strong> займаються в основному розробкою програмного забезпечення прикладного характеру - гри, бухгалтерські програми, редактори, месенджери і т.п. До сфери їхньої роботи також можна віднести створення програмного забезпечення для систем відео- і аудіо-спостереження, систем пожежогасіння або пожежної сигналізації і т.п. Також в їх обов'язки входить адаптація вже існуючих програм під потреби окремо взятої організації або користувача.</li>
    <li><strong>Системні програмісти</strong> розробляють операційні системи, працюють з мережами, пишуть інтерфейси до різних розподілених баз даних. Фахівці цієї категорії відносяться до числа найбільш рідкісних і високооплачуваних. Їх завдання полягає в тому, щоб розробити системи програмного забезпечення (послуги), які, в свою чергу, управляють обчислювальної системою (куди входить процесор, комунікаційні та периферійні пристрої). Також в список завдань входить забезпечення функціонування та роботи створених систем (драйвера пристроїв, завантажувачі і т.д.).</li>
    <li><strong>Web-програмісти</strong> також працюють з мережами, але, в більшості випадків, з глобальними - Інтернет. Вони пишуть програмну складову сайтів, створюють динамічні веб-сторінки, web-інтерфейси для роботи з базами даних.</li>
</ul>