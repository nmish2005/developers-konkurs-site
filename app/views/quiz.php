<?php

namespace app\views;

?>

<h1>Інструмент вибору технологій для Вашого нового проекту</h1>

<div id="quizProgress">
    <div id="first-step" class="num active-step">1</div>

    <div class="progressLine"></div>

    <div id="second-step" class="num">2</div>

    <div class="progressLine"></div>

    <div id="third-step" class="num">3</div>

    <div class="progressLine"></div>

    <div id="fourth-step" class="num">4</div>
</div>

<div class="clearfix"></div>

<form id="quizForm" class="needs-validation">

    <section class="anket">

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio1' name='customRadio' class='custom-control-input' checked>
            <label class='custom-control-label' for='customRadio1'>Сайт-візитка</label>
        </div>

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio2' name='customRadio' class='custom-control-input'>
            <label class='custom-control-label' for='customRadio2'>Інтернет-магазин</label>
        </div>

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio3' name='customRadio' class='custom-control-input'>
            <label class='custom-control-label' for='customRadio3'>Дошка оголошень</label>
        </div>

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio4' name='customRadio' class='custom-control-input'>
            <label class='custom-control-label' for='customRadio4'>Блог</label>
        </div>

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio5' name='customRadio' class='custom-control-input'>
            <label class='custom-control-label' for='customRadio5'>Landing-сторінка</label>
        </div>

        <button class="btn btn-outline-success" id="quizSubmit1" type="button">Далі <i
                    class="fa fa-angle-right"></i>
        </button>
    </section>

    <section class="anket" style="display: none;">

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio6' name='customRadio1' class='custom-control-input' checked>
            <label class='custom-control-label' for='customRadio6'>З фірмовим стилем</label>
        </div>

        <div class='custom-control custom-radio'>
            <input type='radio' id='customRadio7' name='customRadio1' class='custom-control-input'>
            <label class='custom-control-label' for='customRadio7'>Шаблонний дизайн</label>
        </div>

        <div class='custom-control custom-checkbox'>
            <input type='checkbox' id='customCheckbox' name='customCheckbox' class='custom-control-input'>
            <label class='custom-control-label' for='customCheckbox'>Адаптивний дизайн</label>
        </div>

        <button class="btn btn-outline-success" id="quizSubmit2" type="button">Далі <i
                    class="fa fa-angle-right"></i>
        </button>
    </section>

    <section class="anket" style="display: none;">

        <div class='custom-control custom-checkbox'>
            <input type="checkbox" id="customCheckbox1" class="custom-control-input">
            <label class='custom-control-label' for='customCheckbox1'>Авторизація користувачів</label>
        </div>

        <div class='custom-control custom-checkbox'>
            <input type="checkbox" id="customCheckbox2" class="custom-control-input">
            <label class='custom-control-label' for='customCheckbox2'>Форум</label>
        </div>

        <div class='custom-control custom-checkbox'>
            <input type="checkbox" id="customCheckbox3" class="custom-control-input">
            <label class='custom-control-label' for='customCheckbox3'>Зворотній зв'язок</label>
        </div>

        <div class='custom-control custom-checkbox'>
            <input type="checkbox" id="customCheckbox4" class="custom-control-input">
            <label class='custom-control-label' for='customCheckbox4'>Каталог товарів</label>
        </div>

        <div class="custom-control custom-checkbox">
            <input type="checkbox" id="customCheckbox6" class="custom-control-input">
            <label class='custom-control-label' for='customCheckbox6'>Унікальний функціонал (розрахунок вартості, підбір
                товарів тощо)</label>
        </div>

        <button class="btn btn-outline-success" id="quizSubmit3" type="button">Отримати результати <i
                    class="fa fa-angle-right"></i>
        </button>
    </section>

    <div class="clearfix"></div>

</form>