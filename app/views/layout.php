<?php

namespace app\views;

use app\core\App;
use app\core\Settings;
use app\models\Professions;
use app\models\Tech;

?>

<!DOCTYPE html>
<html>
<head>
    <title><?= Settings::getInstance()->getParamFromSettings('projectName') ?></title>
    <meta charset="UTF-8">
    <?= App::genCss() ?>
    <?= App::genLess() ?>
    <script id="dsq-count-scr" src="//mike-sudoku-in-ua-developers-konkurs-site.disqus.com/count.js" async></script>
    <?= App::genJs() ?>
</head>
<body onselectstart="return false;">

<div id="scrollBar">
    <div id="scroll_progress"></div>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand"
           href="/developers-konkurs-site"><?= Settings::getInstance()->getParamFromSettings('projectName') ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" id="home" href="/developers-konkurs-site">Домашня сторінка <span
                                class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="techDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Технології
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php

                        $output = "";
                        $allTechnologies = Tech::getTechnologies();

                        for ($i = 0; $i < count($allTechnologies['name']) + 1; $i++)
                        $output .= "<a class='dropdown-item' href='{$allTechnologies['href'][$i]}'>{$allTechnologies['name'][$i]}</a>";

                        echo $output;
                        ?>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="profDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Професії
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php

                        $output = "";
                        $allProfessions = Professions::getProfessions();

                        for ($i = 0; $i < count($allProfessions['name']) + 1; $i++)
                            $output .= "<a class='dropdown-item' href='{$allProfessions['href'][$i]}'>{$allProfessions['name'][$i]}</a>";

                        echo $output;
                        ?>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="quiz" href="/developers-konkurs-site/main/quiz">Інструмент вибору
                        технологій</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="content" class="container">
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/developers-konkurs-site/app/views/' . $fileName . '.php';
    ?>
</section>

<footer>

    <div class="container">

        <div class="row">

            <div class="col-sm-12">

                <div class="about">

                    <p>Цей сайт створений для IT-шників-початківців. Він допоможе Вам ознайомитись з сучасними
                        IT-професіями та актуальними на цей час технологіями.
                        Нехай цей сайт допоможе молодим IT-мрійникам
                        приєднатись до цікавого світу IT найефективнішим шляхом.</p>

                    <div class="social-media">

                        <ul class="list-inline">

                            <li><a href="https://www.facebook.com/" target="_blank" title=""><i
                                            class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/" target="_blank" title=""><i
                                            class="fa fa-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/?hl=uk" target="_blank" title=""><i
                                            class="fa fa-google-plus"></i></a>
                            </li>
                            <li><a href="https://www.linkedin.com/" target="_blank" title=""><i
                                            class="fa fa-linkedin"></i></a></li>

                        </ul>

                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-6">

                <div class="footer-info-single">

                    <h2 class="title">Домашня сторінка</h2>

                    <ul class="list-unstyled">

                        <li><a href="/developers-konkurs-site/#firstBlock" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Вступ</a></li>
                        <li><a href="/developers-konkurs-site/#professions" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Професії</a></li>
                        <li><a href="/developers-konkurs-site/#techSection" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Технології</a></li>
                        <li><a href="/developers-konkurs-site/#aboutMe" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Про мене</a></li>

                    </ul>

                </div>

            </div>

            <div class="col-md-3 col-sm-6">

                <div class="footer-info-single">

                    <h2 class="title">Технології</h2>

                    <ul class="list-unstyled">

                        <li><a href="/developers-konkurs-site/tech/web" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Веб-розробка</a></li>
                        <li><a href="/developers-konkurs-site/tech/mobile" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Мобільна розробка</a></li>
                        <li><a href="/developers-konkurs-site/tech/system" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Системна розробка</a></li>

                    </ul>

                </div>

            </div>

            <div class="col-md-3 col-sm-6">

                <div class="footer-info-single">

                    <h2 class="title">Професії</h2>

                    <ul class="list-unstyled">

                        <li><a href="/developers-konkurs-site/professions/programmer" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Програміст</a></li>
                        <li><a href="/developers-konkurs-site/professions/tester" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Тестувальник</a></li>
                        <li><a href="/developers-konkurs-site/professions/sysAdmin" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                Системний адміністратор</a></li>
                        <li><a href="/developers-konkurs-site/professions/itSupport" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                IT-підтримка</a></li>
                        <li><a href="/developers-konkurs-site/professions/hrSpec" title=""><i
                                        class="fa fa-angle-double-right"></i>
                                HR-спеціаліст</a></li>

                    </ul>

                </div>

            </div>

            <div class="col-md-3 col-sm-6">

                <div class="footer-info-single">

                    <h2 class="title">Інструмент вибору технологій</h2>

                    <p>Якщо ви починаєте новий проект, обов'язково скористайтеся нашим інструментом вибору технологій.
                        Згідно вибраних
                        відповідей на питання він допоможе Вам з вибором мови програмування та технологій. <a
                                href="/developers-konkurs-site/main/quiz">Перейти &raquo;</a></p>

                </div>

            </div>

        </div>

    </div>

    <section class="copyright">

        <div class="container">

            <div class="row">

                <div class="col-sm-6">

                    <p><?= Settings::getInstance()->getParamFromSettings('projectName') ?> © <?= date('Y') ?>.</p>

                </div>

                <div class="col-sm-6"></div>

            </div>

        </div>

    </section>

</footer>

</body>
</html>