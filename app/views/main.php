<?php

namespace app\views;

?>

<section id="firstBlock">

    <div class="navischo-znaty-programuvannya rounded img-fluid">

        <h1>Вибір професії</h1>

    </div>


    <div id="mainText">

        Цей сайт створений для IT-шників-початківців. Він допоможе Вам ознайомитись з сучасними IT-професіями та
        актуальними на цей час технологіями.
        Насамперед ми намагались приділити увагу, які саме технології використовуються сьогодні для створення якісного
        програмного продукту.
        Якщо ви є професіоналом в IT і використовуєте технології, які не згадані на цьому сайті, будь ласка,
        скористуйтесь нашим форумом,
        ми внесемо правки до відповідних сторінок.
        <br>
        Нехай цей сайт допоможе молодим IT-мрійникам приєднатись до цікавого світу IT найефективнішим шляхом.
        Також ви можете скористатись <i class="bold">зручним <a href="#whereisToBegin">інструментом</a>
            вибору технологій</i> для вашого нового проекту.

    </div>

</section>

<section id="professions">
    <div class="row">
        <div class="card col-lg-4 col-md-4 col-sm-12">

            <img class="card-img-top" src="/developers-konkurs-site/assets/img/developer.jpg"
                 alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Програміст</h5>
                <p class="card-text">Фахівець, що займається програмуванням, виконує розробку програмного
                    забезпечення (в простіших випадках — окремих програм) для програмованих пристроїв, які, як
                    правило містять один чи більше CPU.</p>
                <a href="/developers-konkurs-site/professions/programmer" class="btn btn-outline-primary">Читати
                    більше</a>
            </div>

        </div>

        <div class="card col-lg-4 col-md-4 col-sm-12">

            <img class="card-img-top" src="/developers-konkurs-site/assets/img/tester.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Тестувальник</h5>
                <p class="card-text">Тестувальник є особою, відповідальною за якісне та своєчасне виконання
                    дорученої йому роботи в проекті розробки інформаційно-програмної системи.</p>
                <a href="/developers-konkurs-site/professions/tester" class="btn btn-outline-primary">Читати
                    більше</a>
            </div>

        </div>

        <div class="card col-lg-4 col-md-4 col-sm-12">

            <img class="card-img-top" src="/developers-konkurs-site/assets/img/main.hr_spec.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">HR-спеціаліст</h5>
                <p class="card-text">Найчастіше офіс-менеджер-адміністратор, що займається усім, від замовлення
                    квитків і готелів до організації корпоративу.</p>
                <a href="/developers-konkurs-site/professions/hrSpec" class="btn btn-outline-primary">Читати
                    більше</a>
            </div>

        </div>

        <div class="card col-lg-4 col-md-4 col-sm-12">

            <img class="card-img-top" src="/developers-konkurs-site/assets/img/main.sysAdmin.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Системний адміністратор</h5>
                <p class="card-text">Працівник, посадові обов'язки якого передбачають забезпечення роботи
                    комп'ютерної техніки, комп'ютерної мережі і програмного забезпечення в організації.</p>
                <a href="/developers-konkurs-site/professions/sysAdmin" class="btn btn-outline-primary">Читати
                    більше</a>
            </div>

        </div>

        <div class="card col-lg-4 col-md-4 col-sm-12">

            <img class="card-img-top" src="/developers-konkurs-site/assets/img/it-support.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">IT-підтримка</h5>
                <p class="card-text">Виділений підрозділ в організації, що займаються обробкою звернень та
                    інформуванням в інтересах організації-замовника або головної організації.</p>
                <a href="/developers-konkurs-site/professions/itSupport" class="btn btn-outline-primary">Читати
                    більше</a>
            </div>

        </div>
    </div>

</section>

<section id="techSection">
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12 techList">
            <img src="/developers-konkurs-site/assets/img/client-server.svg" alt="client-server icon">
            <br>
            <h5>Web-розробка</h5>

            <ul class="birds">
                <li>JavaScript</li>
                <li>PHP</li>
                <li>HTML</li>
                <li>CSS</li>
                <li>MVC</li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 techList">
            <img src="/developers-konkurs-site/assets/img/mobile-server.gif" alt="mobile-server icon">
            <br>
            <h5>Мобільна розробка</h5>

            <ul class="birds">
                <li>Java</li>
                <li>Objective-C</li>
                <li>React Native</li>
                <li>Swift</li>
                <li>Qt</li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 techList">
            <img src="/developers-konkurs-site/assets/img/pc.png" alt="pc icon">
            <br>
            <h5>Системна розробка</h5>

            <ul class="birds">
                <li>C/C++</li>
                <li>.NET</li>
                <li>Assembler</li>
            </ul>
        </div>
    </div>
</section>

<section class="toolBlock" id="whereisToBegin">
    <div class="container py-3">
        <div class="row">
            <div class="wikiLogo col-lg-1 col-md-1 col-sm-12">
                <img src="/developers-konkurs-site/assets/img/tool.png" class="w-100">
            </div>
            <div class="col-lg-11 col-md-11 col-sm-12">
                <div>
                    <h5>Інструмент вибору технологій</h5>
                    <p>Якщо ви починаєте новий проект, обов'язково скористайтеся нашим інструментом вибору технологій. Згідно вибраних
                        відповідей на питання він допоможе Вам з вибором мови програмування та технологій.</p>
                    <div class="rightAligned">
                        <a href="main/quiz"
                           target="_blank">Почати &raquo;</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="aboutMe">

    <h2>Про мене</h2>

    <div class="row">
        <div class="col-6">

            <span class="monospace">технології та інструменти</span>
            <br>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <ul class="tech">
                        <li><img src="/developers-konkurs-site/assets/img/icons/html5.png" alt="icon"> HTML5</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/css3.svg" alt="icon"> CSS3</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/js.svg" class="rounded" alt="icon">
                            JavaScript
                        </li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/less.png" alt="icon"> Less</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/php.png" alt="icon"> PHP</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/wp.png" alt="icon"> Wordpress</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/fa.png" alt="icon"> Font Awesome</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/mysql.png" alt="icon"> MySQL</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/gimp.png" alt="icon"> GIMP</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/bs.png" alt="icon"> Bootstrap</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/phpstorm.png" alt="icon"> PhpStorm</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/api.png" alt="icon"> API</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <ul class="tech">
                        <li><img src="/developers-konkurs-site/assets/img/icons/java.png" alt="icon"> Java</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/jq.png" alt="icon"> jQuery</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/xml.png" alt="icon"> XML</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/drupal.svg" alt="icon"> Drupal</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/json.png" alt="icon"> JSON</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/mvc.png" alt="icon"> MVC</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/animate.png" alt="icon"> Animate.css
                        </li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/code.png" alt="icon"> Singleton</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/react.png" alt="icon"> ReactJS</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/go.png" alt="icon"> GoJS</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/ajax.png" alt="icon"> AJAX</li>
                        <li><img src="/developers-konkurs-site/assets/img/icons/swal.svg" alt="icon"> SweetAlert</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12"></div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <ul class="tech">
                        <li><img src="/developers-konkurs-site/assets/img/icons/git.png" alt="icon"> Git</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12"></div>
            </div>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <span class="monospace">персональна інформація</span>

            <br><br>
<p>
            Я займаюсь програмуванням вже більше року. Спочатку я створював блог на CMS, але потім довелося створити
            маленьку самописну веб-сторінку. З тих пір мені надзвичайно сподобався веб-дизайн, а згодом я почав
            програмувати на PHP. Це було набагато цікавіше. Тепер я розвиваюся у всіх напрямках: від невеликих
            веб-додатків до навіть програмування для Android та створення комп'ютерних програм. Після PHP я почав
            вивчати СУБД MySQL, а згодом і інші. Після PHP я вирішив більше розібратись в 'Front-end' - програмування на
            стороні клієнта.
</p>
        </div>
    </div>

</section>

<section id="freeFood">
    <h3>P.S.</h3>
    <p>...І, мабуть, найголовнішим у великих IT-компаніях є <i>надання</i> працівникам компанії <i>житла</i>, <i>їжі</i>
        і т.д. <strong>за рахунок компанії</strong> ;-)</p>
</section>