<?php

namespace app\views\errors;


use app\core\Settings;

?>
<h1>

<i class="fa fa-low-vision"></i> Сторінка з введеною адресою не знайдена.
<br>

Будь ласка, перейдіть до <a href="<?=Settings::getInstance()->getParamFromSettings('siteUrl')?>">головної сторінки</a>.

</h1>