<?php

namespace app\models;


use app\core\DBase;

class Tech
{

    static function getTechnologies()
    {
        $arr = [];
        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT * FROM tech");

        while ($row = mysqli_fetch_assoc($query)) {
            $arr['name'][] = $row['name'];
            $arr['href'][] = $row['href'];
        }

        return $arr;
    }

}