<?php

namespace app\models;


use app\core\DBase;

class Professions
{

    static function getProfessions()
    {
        $arr = [];
        $query = mysqli_query(DBase::getInstance()->getLink(), "SELECT * FROM professions");

        while ($row = mysqli_fetch_assoc($query)) {
            $arr['name'][] = $row['name'];
            $arr['href'][] = $row['href'];
        }

        return $arr;
    }

}