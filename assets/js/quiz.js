var results = {type: '', design: [], func: []};

$(function () {

    $("#quizSubmit1").click(function () {

        $('.anket:nth-child(1)').css('display', 'none');

        $('#quizProgress #first-step').removeClass('active-step');

        $('.anket:nth-child(2)').css('display', 'block');

        $('#quizProgress #second-step').addClass('active-step');

        $(".anket:nth-child(3) .custom-control").show();

        results.type = $(".anket:nth-child(1) input:checked+label").text();

        switch (results.type) {
            case "Сайт-візитка":
                $(".anket:nth-child(3) div:nth-child(1), .anket:nth-child(3) div:nth-child(2), .anket:nth-child(3) div:nth-child(4), .anket:nth-child(3) div:nth-child(5), .anket:nth-child(3) div:nth-child(6)").hide();
                break;

            case 'Блог':
                $(".anket:nth-child(3) div:nth-child(1), .anket:nth-child(3) div:nth-child(4), .anket:nth-child(3) div:nth-child(5), .anket:nth-child(3) div:nth-child(6)").hide();
                break;

            case 'Landing-сторінка':
                $(".anket:nth-child(3) div:nth-child(1), .anket:nth-child(3) div:nth-child(2), .anket:nth-child(3) div:nth-child(4), .anket:nth-child(3) div:nth-child(5), .anket:nth-child(3) div:nth-child(6)").hide();
                break;
        }
    });

    $("#quizSubmit2").click(function () {
        $('.anket:nth-child(2)').css('display', 'none');

        $('#quizProgress #second-step').removeClass('active-step');

        $('.anket:nth-child(3)').css('display', 'block');

        $('#quizProgress #third-step').addClass('active-step');

        $(".anket:nth-child(2) input:checked+label").each(function () {
            results.design.push($(this).text());
        });

    });

    $("#quizSubmit3").click(function () {
        $(".anket:nth-child(3) input:checked+label").each(function () {
            results.func.push($(this).text());
        });

        post('http://mike.sudoku.in.ua/developers-konkurs-site/main/quizResults', {results: JSON.stringify(results)});
    });


    $("#first-step").click(function () {
        $('.anket').css('display', 'none');

        $('#quizProgress *').removeClass('active-step');

        $('.anket:nth-child(1)').css('display', 'block');

        $('#quizProgress #first-step').addClass('active-step');

    });

    $("#second-step").click(function () {
        $('.anket').css('display', 'none');

        $('#quizProgress *').removeClass('active-step');

        $('.anket:nth-child(2)').css('display', 'block');

        $('#quizProgress #second-step').addClass('active-step');
    });

    $("#third-step").click(function () {
        $('.anket').css('display', 'none');

        $('#quizProgress *').removeClass('active-step');

        $('.anket:nth-child(3)').css('display', 'block');

        $('#quizProgress #third-step').addClass('active-step');
    });

});

function post(path, params, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}