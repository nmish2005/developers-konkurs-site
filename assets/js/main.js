var scrolled = '0%';

function addActive() {
    const uriComponents = location.pathname.toString().split('/');

switch (uriComponents[2]) {
    case 'tech':
        $("#home").parent().removeClass('active');
        return $("#techDropdown").parent().addClass('active');

    case 'professions':
        $("#home").parent().removeClass('active');
        return $("#profDropdown").parent().addClass('active');
}

if (uriComponents[3] == 'quiz'){
    $("#home").parent().removeClass('active');
    return $('#quiz').parent().addClass('active');
}

return false;

}

$(function () {

    addActive();

    scrolled = (document.documentElement.scrollTop / (document.documentElement.scrollHeight - document.documentElement.clientHeight)) * 100 + '%';

    $("#scroll_progress").animate({ width: scrolled }, 500);

    $(window).scroll(function () {
        scrolled = (document.documentElement.scrollTop / (document.documentElement.scrollHeight - document.documentElement.clientHeight)) * 100 + '%';
        $("#scroll_progress").css('width', scrolled);
    });

    $("a[href^='#']").click(
        function (event) {
            if ($(this).attr('href') == '#')
                return false;

            //отменяем стандартную обработку нажатия по ссылке
            event.preventDefault();

            //забираем идентификатор бока с атрибута href
            const id = $(this).attr('href'),

                //узнаем высоту от начала страницы до блока на который ссылается якорь
                top = $(id).offset().top - $("nav").height() - 15;

            //анимируем переход на расстояние - top за 2с
            $('body,html').animate({scrollTop: top}, 2000);

            return true
        }
    );


});